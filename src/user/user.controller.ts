import {
	Controller,
	Get,
	Inject,
	OnModuleInit
} from "@nestjs/common";
import { ClientGrpc } from "@nestjs/microservices";

@Controller("user")
export class UserController implements OnModuleInit {

	constructor(
		@Inject("UserClient")
		private client: ClientGrpc
	) {
	}

	private userService: any;

	public onModuleInit(): void {
		this.userService = this.client.getService<any>("UserService");
	}

	@Get()
	public async getUsers() {
		return this.userService.getUsers({});
	}
}
