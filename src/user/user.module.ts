import { Module } from "@nestjs/common";
import { DoubleClientModule } from "@solar/double-client";
import { UserController } from "./user.controller";

@Module({
	imports: [
		DoubleClientModule.register({
			grpcUrl: "localhost:5000",
			protoUrl: "http://localhost:3000/proto",
			injectionTokenName: "UserClient",
			generateInterfaces: true
		})],
	controllers: [UserController]
})
export class UserModule {
}
